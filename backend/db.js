const { MongoClient, ObjectId } = require('mongodb')
const moment = require('moment')
const momenttimezone = require('moment-timezone')
const { url, dbName, coachTimezone } = require('./config')
let db;

const init = () =>
    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).then((client) => {
        db = client.db(dbName)
    })
const removeDocuments = () => {
    const collection = db.collection('events')
    collection.deleteMany()
}
const getAllBookingSlots = () => {
    const bookingSlots = db.collection('booking-slots')
    return bookingSlots.find({}).toArray()
}
const getFreeSlots = (date) => {

    const freeSlots = db.collection('events')

    /** Steps to find the free slots
     * 1. convert users date to coach timezone
     * 2. find event with that particular date
     * 3. if that date is available then get the timezone which are available (free) and return
     * 4. if that date is not available return all slots
     */
    let currentDate = momenttimezone().tz(coachTimezone).format("YYYY-MM-DD"), filter;
    let differenceInDays = moment(currentDate).diff(moment(date), 'days');
    return new Promise(async (resolve, reject) => {
        if (differenceInDays >= 1) {
            resolve({ message: 'DAY_PASSED', success: false })
        }
        else if (differenceInDays === 0) {
            // day is current day as per coach timezone
            filter = {
                date
            }
            let items = await freeSlots.find(filter).toArray(), bookedSlotId = []
            items && items.length !== 0 && items.map(item => {
                bookedSlotId.push(ObjectId(item.slotId))
            })
            let allBookingSlots = db.collection('booking-slots')

            let availableSlots = await allBookingSlots.find({
                _id: {
                    $nin: bookedSlotId
                }
            }).toArray()
            resolve({ items: availableSlots, success: true })
        }
        else if (differenceInDays < 0) {
            let items = await getAllBookingSlots()
            resolve({ items, success: true })
        }
        else {
            reject({ success: false, message: "ERROR" })
        }
    })

}
const getEvents = (startDate, endDate) => {
    let dates = enumerateDaysBetweenDates(startDate, endDate)
    const collection = db.collection('events')
    return collection.find({
        date: { $in: dates }
    }).toArray()
}
const getAllEvents = () => {
    const collection = db.collection('events')
    return collection.find({}).toArray()
}
const createEvent = async (date, slotId, timezone) => {
    const collection = db.collection('events')
    try {
        let isEventPresent = await collection.findOne({
            date,
            slotId,
            timezone
        })

        if (isEventPresent) {
            return ({ success: false, message: "Coach already booked" })
        }
        else {
            let res = await collection.insertOne({
                date,
                slotId,
                timezone
            })
            return ({ success: true, message: "Coach booked successfully" })

        }


    }
    catch (error) {
        console.error(`Something went wrong: ${err}`)
        return ({ success: false, message: "Error while booking coach" })
    }
    // return collection.update({
    //     date,
    //     slotId,
    //     timezone
    // }, {
    //     date,
    //     slotId,
    //     timezone
    // }, {
    //     "upsert": true
    // })
    //     .then(
    //         res => {
    //             console.log(`Updated ${res.result.n} documents`)
    //             return ({ success: true, message: "Coach booked successfully" })
    //         },
    //         err => {
    //             console.error(`Something went wrong: ${err}`)
    //             return ({ success: false, message: "Error while booking coach" })
    //         },
    //     );
}

const enumerateDaysBetweenDates = (startDate, endDate) => {
    startDate = moment(startDate);
    endDate = moment(endDate);

    var now = startDate, dates = [];

    while (now.isBefore(endDate) || now.isSame(endDate)) {
        dates.push(now.format('YYYY-MM-DD'));
        now.add(1, 'days');
    }
    return dates;
};
module.exports = { init, getEvents, removeDocuments, getFreeSlots, createEvent, getAllEvents, getEvents,getAllBookingSlots }
