const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors')

const { init,insertInitalEvents } = require('./db')
const routes = require('./routes')
const app = express();
const port = process.env.PORT || 8081;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())

//routes
app.use('/api', routes)


init().then(async () => {
    let server = app.listen(port, function () {
        var host = server.address().address
        var port = server.address().port
        console.log("Appointment booking app listening at http://%s:%s", host, port)
    })
})

