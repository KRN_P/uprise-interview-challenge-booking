require('dotenv').config()
module.exports = {
  url: process.env.DB_URL,
  dbName: process.env.DB_NAME,
  coachTimezone:process.env.COACH_TIMEZONE
};