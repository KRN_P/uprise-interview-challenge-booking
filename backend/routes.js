const { ObjectId } = require('mongodb')

const express = require('express')
const { getEvents, removeDocuments, getFreeSlots, createEvent, getAllEvents, getAllBookingSlots } = require('./db')
const { coachTimezone } = require('./config')
const router = express.Router()

router.get('/all', async (req, res) => {
    try {
        let response = await getAllEvents()
        if (response && response.length !== 0)
            res.status(200).json({ events: response, success: true })
        else res.status(200).json({ events: [], success: true, message: "No events available" })
    }
    catch (error) {
        console.log(error)
        res.status(500).end()
    }
})
router.post('/getEvents', async (req, res) => {
    const { startDate, endDate } = req.body;
    if (!startDate) {
        res.status(400).json({ "error": "[startDate] parameter is required", "success": false })
        return
    }
    if (!endDate) {
        res.status(400).json({ "error": "[endDate] parameter is required", "success": false })
        return
    }
    try {
        let bookingSlots = await getAllBookingSlots();
        let response = await getEvents(startDate, endDate)
        if (response && response.length !== 0) {
            let events = []
            response.map(e => {
                bookingSlots.map(b => {
                    if (e.slotId.toString() === b._id.toString()) {
                        events.push({
                            id: e._id,
                            date: e.date,
                            slotId: e.slotId,
                            timezone: e.timezone,
                            startTime: b.startTime,
                            endTime: b.endTime
                        })
                    }
                })
            })
            res.status(200).json({ events, success: true })
        }

        else res.status(200).json({ events: [], success: true, message: "No events available" })
    }
    catch (error) {
        console.log(error)
        res.status(500).end()
    }
})
router.post('/freeslots', async (req, res) => {
    const { date, timezone } = req.body;
    if (!date) {
        res.status(400).json({ "error": "[date] parameter is required", "success": false })
        return;
    }
    if (!timezone) {
        res.status(400).json({ "error": "[timezone] parameter is required", "success": false })
        return;
    }
    try {
        let response = await getFreeSlots(date)
        let freeSlots = []
        if (response.success && response.items && response.items.length !== 0) {
            freeSlots = response.items;
            res
                .status(200)
                .json({
                    "availableSlots": freeSlots,
                    "success": true,
                    "error": ""
                })
        }

        else if (!response.success && response.message === 'DAY_PASSED') {
            res
                .status(400)
                .json({
                    "availableSlots": [],
                    success: false,
                    error: `Cannot book event as day has already passed in ${coachTimezone} timezone`
                })
        }
        else {
            res
                .status(400)
                .json({
                    "availableSlots": [],
                    success: false,
                    error: `Error while fetching free slots`
                })
        }
    }
    catch (error) {
        console.log(error)
        res.status(500).end()
    }

})
router.post('/create', async (req, res) => {
    const { date, timezone, slotId } = req.body;
    if (!date) {
        res.status(400).json({ "error": "[date] parameter is required", "success": false })
        return;
    }
    if (!slotId) {
        res.status(400).json({ "error": "[slotId] parameter is required", "success": false })
        return
    }
    if (!timezone) {
        res.status(400).json({ "error": "[timezone] parameter is required", "success": false })
        return;
    }
    try {
        let response = await createEvent(date, slotId, timezone)
        if (response && response.success) {
            res
                .status(200)
                .json({
                    success: true,
                    message: response.message,
                    error: ``
                })
        }
        else if (response && !response.success) {
            res
                .status(422)
                .json({
                    success: false,
                    message: response.message,
                    error: ``
                })
        }
        else {
            res
                .status(400)
                .json({
                    success: false,
                    message: '',
                    error: `Error while booking coach`
                })
        }

    }
    catch (error) {
        console.log(err)
        res.status(500).end()
    }

})
router.get('/remove', async (req, res) => {
    await removeDocuments()
    res.status(200).json({ "success": true, "error": "" })
})
module.exports = router
