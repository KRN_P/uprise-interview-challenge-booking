import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
const BookCoachScreen = React.lazy(() => import('../screens/BookCoach'));
const EventListScreen = React.lazy(() => import('../screens/EventList'));
const SlotsScreen = React.lazy(() => import('../screens/Slots'));

const Routes = () => {
    console.log('heres')
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/book" />
                    </Route>
                    <Route exact path={'/book'} component={BookCoachScreen} />
                    <Route exact path={'/slots'} component={SlotsScreen} />
                    <Route exact path={'/list'} component={EventListScreen} />
                </Switch>
            </Router>
        </Suspense>
    )
}
export default Routes;