export const BASE_URL = `http://localhost:8081/api/`;
export const GET_ALL_SLOTS = `${BASE_URL}freeslots`
export const SAVE_EVENT = `${BASE_URL}create`
export const GET_EVENTS = `${BASE_URL}getEvents`