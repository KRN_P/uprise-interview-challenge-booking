import React from 'react';
import Routes from './helpers/Routes'
import { Provider } from "react-redux";
import configureStore from "./redux/Store";

let store = configureStore({ preloadedState: {} });

const App = () => {
  return (
    <Provider store={store}>
      <React.Fragment>
        <Routes />
      </React.Fragment>
    </Provider>
  )
}
export default App;