import {
    SAVE_DATE_TIME_ZONE
} from '../ActionConstant'
import {
    GET_ALL_SLOTS,
    GET_EVENTS,
    SAVE_EVENT
} from "../../helpers/Config"
import axios from 'axios'

export const saveDateTimezone = (date, timezone) => {
    return dispatch => {
        dispatch({
            type: SAVE_DATE_TIME_ZONE,
            payload: {
                date,
                timezone
            }
        })
    }
}
export const getSlots = () => {
    return async (dispatch, getState) => {
        const { date, timezone } = getState().EventReducer
        return axios({
            url: GET_ALL_SLOTS,
            method: "POST",
            data: {
                date, timezone
            }
        })
            .then(response => {
                return response
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            })
    }
}
export const bookCoach = (slotId) => {
    return async (dispatch, getState) => {
        const { date, timezone } = getState().EventReducer
        return axios({
            url: SAVE_EVENT,
            method: "POST",
            data: {
                date, timezone, slotId
            }
        })
            .then(response => {
                return response
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            })
    }
}
export const getEvents = (date) => {
    return async (dispatch, getState) => {
        return axios({
            url: GET_EVENTS,
            method: "POST",
            data: {
                startDate: date,
                endDate: date
            }
        })
            .then(response => {
                return response
            })
            .catch(error => {
                console.log(error.response);
                return error.response;
            })
    }
}