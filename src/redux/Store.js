import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import rootReducer from "../redux/reducers/";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


const persistConfig = {
    key: 'common',
    storage: storage,
};
const pReducer = persistReducer(persistConfig, rootReducer);
const configureAppStore = ({ preloadedState }) => {
    let env = process.env.REACT_APPs_BUILD || 'dev';

    const store = configureStore({
        reducer: pReducer,
        devTools: env === 'production' ? false : true,
        middleware:
            env !== "production"
                ? [...getDefaultMiddleware()]
                : [...getDefaultMiddleware()],
        preloadedState,
    });
    // const persistor = persistStore(store);
    return store;
};

export default configureAppStore;
