import {
    SAVE_DATE_TIME_ZONE
} from '../ActionConstant'

let initialState = {
    date: null,
    timezone: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SAVE_DATE_TIME_ZONE: {
            return {
                ...state,
                date: action.payload.date,
                timezone: action.payload.timezone
            }
        }
        default: return state;
    }
}