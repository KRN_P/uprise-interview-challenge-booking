import EventReducer from "./EventReducer"
import { combineReducers } from 'redux'

const RootReducer = combineReducers({
    EventReducer
})
export default RootReducer;