import React, { useEffect, useState } from 'react'
import { Container, Card, GridColumn, GridRow, Label, BackButton, BookedIcon } from './StyledComponents'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faCheck } from '@fortawesome/free-solid-svg-icons'
import { getSlots } from "../redux/actions/EventAction"
import { useDispatch, useSelector } from 'react-redux'
import momenttimezone from 'moment-timezone'
import { bookCoach } from '../redux/actions/EventAction'

const Slots = (props) => {
    const dispatch = useDispatch()
    const [selectedSlot, setSelectedSlot] = React.useState()
    const [slots, setSlots] = useState([])
    const [isEventBooked, setIsEventBooked] = useState(false)
    const { timezone, date } = useSelector(state => state.EventReducer)
    useEffect(() => {
        getFreeSlots()
    }, [])
    const getFreeSlots = async () => {
        let response = await dispatch(getSlots());
        if (response && response.data && response.data.success) {
            let slots = response.data.availableSlots, convertedSlots = [];
            slots && slots.length !== 0 && slots.map(slot => {
                let startTime = `${date} ${slot.startTime}`,
                    endTime = `${date} ${slot.endTime}`;

                startTime = momenttimezone.tz(startTime, 'YYYY-MM-DD hh:mm', 'Australia/Sydney');
                endTime = momenttimezone.tz(endTime, 'YYYY-MM-DD hh:mm', 'Australia/Sydney');

                startTime = momenttimezone(startTime, ['YYYY-MM-DD hh:mm A']).tz(timezone).format("hh:mm A");
                endTime = momenttimezone(endTime, ['YYYY-MM-DD hh:mm A']).tz(timezone).format("hh:mm A");

                convertedSlots.push({
                    slotId: slot._id,
                    slot: `${startTime} - ${endTime}`
                })
            })
            setSlots(convertedSlots)
        }
    }


    const renderSlots = () => {
        return (
            <GridRow>
                {
                    slots.map((item, index) => {
                        return <GridColumn
                            active={index === selectedSlot ? true : false}
                            onClick={() => onSlotItemClick(index, item.slotId)} key={index}>
                            {item.slot}
                        </GridColumn>
                    })
                }

            </GridRow>
        )
    }
    const onSlotItemClick = async (index, slotId) => {
        setSelectedSlot(index)
        let response = await dispatch(bookCoach(slotId))
        if (response && response.data && response.data.success) {
            getFreeSlots()
            setSelectedSlot('')
            setIsEventBooked(true)
        }
        else {
            // error while booking coach
            alert('Error while booking coach, please try again')
        }
    }
    const renderBackButton = () => {
        return (
            <BackButton onClick={onBackClick}>
                <FontAwesomeIcon icon={faArrowLeft} /> Back
            </BackButton>
        )
    }
    const onBackClick = () => {
        props.history.goBack()
    }
    return (
        <Container>
            {
                !isEventBooked &&
                <Card>
                    {renderBackButton()}
                    <Label>Click on any of the item slots to book</Label>
                    {renderSlots()}
                </Card>
            }
            {
                isEventBooked &&
                <Card>
                    {renderBackButton()}
                    <BookedIcon>
                        <FontAwesomeIcon icon={faCheck} />

                    </BookedIcon>
                    <Label style={{ textAlign: "center" }}>Booking Successfull</Label>
                </Card>
            }

        </Container>
    )
}

export default Slots
