import React, { useState } from 'react'
import moment from "moment";

import 'react-dates/initialize';
import { DayPickerSingleDateController } from 'react-dates';
import { Container, Card, LeftSection, RightSection, Label, AppointmentButton, Row, DropDown, ErrorMessage } from './StyledComponents'
import { useDispatch } from 'react-redux';
import { saveDateTimezone } from '../redux/actions/EventAction';

const BookCoach = (props) => {
    const dispatch = useDispatch()
    const timezones = [
        {
            "label": "Calcutta",
            "value": "Asia/Calcutta"
        },
        {
            "label": "Sydney",
            "value": "Australia/Sydney"
        }
    ]
    const [date, setDate] = useState(moment())
    const [selectedTimezone, setSelectedTimezone] = useState(0)
    const [errorMessage, setError] = useState('');

    const onTimezoneSelectChange = (e) => {
        setError('')
        setSelectedTimezone(e.target.value)
    }
    const onGetFreeSlotsClick = () => {
        if (!date) {
            setError('Please select valid date')
            return;
        }
        if (!selectedTimezone || selectedTimezone.toString() === '0') {
            setError('Please select valid timezone')
            return;
        }
        dispatch(saveDateTimezone(moment(date).format("YYYY-MM-DD"), selectedTimezone))
        props.history.push('/slots')
    }
    const renderLeftSection = () => {
        return (
            <LeftSection>
                <Label>Select Date</Label>
                <DayPickerSingleDateController
                    numberOfMonths={1}
                    focused={true}
                    onDateChange={date => {
                        setDate(date);
                        setError('')
                    }}
                    date={date}
                    noBorder
                    hideKeyboardShortcutsPanel
                />
            </LeftSection>
        )
    }
    const renderRightSection = () => {
        return (
            <RightSection>
                <Label>Select your timezone</Label>
                <DropDown
                    onChange={onTimezoneSelectChange}
                    value={selectedTimezone}
                >
                    <option value="0">Select Your Timezone</option>
                    {
                        timezones.map((item, index) => {
                            return <option value={item.value} key={index}>{item.label}</option>
                        })
                    }
                </DropDown>
            </RightSection>
        )
    }
    const renderError = () => {
        return (
            <ErrorMessage>
                {errorMessage}
            </ErrorMessage>
        )
    }
    const onShowEventsPressed = ()=>{
        props.history.push("/list")
    }
    return (
        <Container>
            <Card>
                {errorMessage && renderError()}
                <Row>
                    {renderLeftSection()}
                    {renderRightSection()}
                </Row>
                <Row>
                    <AppointmentButton title="Get Free Slots" variant="primary" size="medium" onClick={onGetFreeSlotsClick} />
                    <AppointmentButton title="Show All Events " variant="primary" size="medium" onClick={onShowEventsPressed} style={{ marginLeft: 10 }} />
                </Row>
            </Card>

        </Container>

    )
}

export default BookCoach
