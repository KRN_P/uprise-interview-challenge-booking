import React, { useState } from 'react'
import moment from "moment";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faCheck } from '@fortawesome/free-solid-svg-icons'
import 'react-dates/initialize';
import { DayPickerSingleDateController } from 'react-dates';
import { Container, Card, LeftSection, RightSection, Label,BackButton, AppointmentButton, Row, EventItem, EventWrapper } from './StyledComponents'
import { useDispatch } from 'react-redux';
import { getEvents } from '../redux/actions/EventAction'
const EventList = (props) => {
    const dispatch = useDispatch()
    const [date, setDate] = useState(moment())
    const [events, setEvents] = useState([])
    const renderLeftSection = () => {
        return (
            <LeftSection>
                <Label>Select Date</Label>
                <DayPickerSingleDateController
                    numberOfMonths={1}
                    focused={true}
                    onDateChange={date => {
                        setDate(date);
                    }}
                    date={date}
                    noBorder
                    hideKeyboardShortcutsPanel
                />
                <AppointmentButton title="Show All Events" variant="primary" size="medium" onClick={onShowAllEventPress} />
            </LeftSection>
        )
    }
    const onShowAllEventPress = async () => {
        if (!date) {
            alert("Please select a date to get events");
            return;
        }
        let response = await dispatch(getEvents(date))
        console.log(response)
        if (response && response.data && response.data.success && response.data.events.length !== 0) {
            setEvents(response.data.events)
        }
        else {
            alert(response.data.message || 'Error while fetching events')
        }
    }
    const renderRightSection = () => {
        console.log(events)
        return (
            <RightSection>
                <Label>Available Events</Label>
                <EventWrapper>
                    {
                        events && events.length !== 0 &&
                        events.map((e, i) => {
                            console.log(e)
                            return (
                                <EventItem key={i}>
                                    {e.startTime} - {e.endTime} (AEST)
                                </EventItem>
                            )
                        })
                    }
                </EventWrapper>
            </RightSection>
        )
    }
    const renderBackButton = () => {
        return (
            <BackButton onClick={onBackClick}>
                <FontAwesomeIcon icon={faArrowLeft} /> Back
            </BackButton>
        )
    }
    const onBackClick = () => {
        props.history.goBack()
    }
    return (
        <Container>
            <Card>
                {renderBackButton()}
                <Label style={{ textAlign: 'center' }}>Show Events</Label>
                <Row>
                    {renderLeftSection()}
                    {events && events.length !== 0 && renderRightSection()}
                </Row>
            </Card>

        </Container>

    )
}

export default EventList
