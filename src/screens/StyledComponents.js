import styled from 'styled-components/macro'
import { Button } from '@uprise/button'
export const Container = styled.div`
    background-color:#f7f7f7;
    display:flex;
    flex:1;
    justify-content:flex-start;
    flex-direction:column;
`
export const Card = styled.div`
    background-color:#ffffff;
    width:700px;
    display:flex;
    align-self:center;
    border-radius:7px;
    box-shadow:0px 1px 1px rgba(0,0,0,.2);
    flex-direction:column;
    padding:20px;
    margin-top:20px;
`
export const Row = styled.div`
    display:flex;
    flex-direction:row;
`
export const LeftSection = styled.div`
    display:flex;
    flex:.5;
    flex-direction:column;
    justify-content:center;
    padding:20px;
`
export const RightSection = styled.div`
    display:flex;
    flex:.5;
    flex-direction:column;
    padding:20px;
`
export const Label = styled.div`
    font-size:14px;
    font-weight:bold;
    margin-top:20px;
    margin-left:20px;
`
export const AppointmentButton = styled(Button)`
    font-family:sans-serif;
    font-size:14px;
    width:70%;
    align-self:center;
    
`
export const DropDown = styled.select`
    padding:10px;
    border:none;
    border-bottom:1px solid #d8d6d6;
    margin-top:10px;
    font-size:14px;
    font-family:sans-serif;
    margin-left:20px;

`
export const GridRow = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin-top:20px;
    align-items:center;
    justify-content:flex-start;
`
export const GridColumn = styled.button`
    font-family:sans-serif;
    flex-basis: 200px;
    padding: 10px;
    margin: 5px;
    justify-content: center;
    align-items: center;
    display: flex;
    border-radius: 5px;
    cursor: pointer;
    font-weight:bold;
    font-size:14px;
    border:none;
    background-color: ${props => props.active ? '#7d60ff' : '#f7f7f7'};
    color:${props => props.active ? '#fff' : '#7d60ff'};
    
`
export const ErrorMessage = styled.div`
    padding: 5px;
    color: red;
    background-color: #ff000017;
    width: 50%;
    align-self: center;
    justify-content: center;
    align-items: center;
    display: flex;
    font-size: 14px;
    font-family: sans-serif;
`
export const BackButton = styled.button`
    border:none;
    background:transparent;
    font-size:14px;
    font-family:sans-serif;
    cursor:pointer;
    align-self:flex-start;
    font-weight:bold;
    font-size: 14px;
    font-family: sans-serif;
`
export const EventWrapper = styled.div`
    height:300px;
    overflow:hidden;
    overflow-y:scroll;
    display:flex;
    flex-direction:column;
`
export const EventItem = styled.div`
    display: flex;
    padding:10px;
    margin-top:10px;
    justify-content:center;
    align-items:center;
    align-self:center;
    min-width:200px;
    background-color:#fff3e3;
    font-size:14px;
    font-weight:bold;
    font-family: sans-serif;
`
export const BookedIcon = styled.div`
    display:flex;
    flex-direction:column;
    width:50px;
    height:50px;
    border-radius:50px;
    align-self:center;
    background-color:#fff3e3;
    justify-content:center;
    align-items:center
`